var Sshare = require('such-social-share');

document.getElementById("facebook").onclick = function() {
    Sshare.facebookShare();
};

document.getElementById("twitter").onclick = function() {
    Sshare.twitterShare('Such wow');
};

document.getElementById("mailto").onclick = function() {
    Sshare.mailTo('test@test.com', 'This is my subject', 'This is my message <br> with br');
};

document.getElementById("google").onclick = function() {
    Sshare.googleShare();
};

document.getElementById("mandrill").onclick = function() {
    var obj = {
        key : 'YourApiKey',
        from_email : 'adrien.espel@gmail.com',
        email : 'espel.adrien@gmail.com',
        name : 'Adrien Espel',
        type : 'to',
        subject : 'hello you',
        content : 'lelele le l lelelel e<br> lelelelele'
    };
    Sshare.mandrillSend(obj, function(data){
        console.log(data);
    });
};