'use strict';

var gulp        = require('gulp');
var runSequence = require('run-sequence');

// Tasks
require('./browserify');

gulp.task('dev', function(callback){

    callback = callback || function() {};
    runSequence([ 'browserify-dev'], callback);

});