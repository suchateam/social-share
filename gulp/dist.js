'use strict';

var gulp        = require('gulp');
var runSequence = require('run-sequence');

// Tasks
require('./browserify');

gulp.task('dist', function(callback){

    callback = callback || function() {};
    runSequence([ 'browserify-dist'], callback);

});