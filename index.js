var request = require('superagent');

module.exports = {

    /**
     * Action for popup spawn
     * @param url
     * @param width
     * @param height
     */
    windowPopup : function(url, width, height) {
        var left = (screen.width / 2) - (width / 2), top = (screen.height / 2) - (height / 2);
        window.open(
            url,
            "",
            "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=" + width + ",height=" + height + ",top=" + top + ",left=" + left
        );
    },

    /**
     * Share Facebook
     */
    facebookShare : function(){
        this.windowPopup('https://www.facebook.com/share.php?u=' + window.location.href, 500, 300);
    },

    /**
     * Share Twitter
     * @param message
     */
    twitterShare : function(message){
        this.windowPopup('https://twitter.com/home?status='+message.trim() + ' ' + window.location.href, 500, 300);
    },

    /**
     * Share google
     */
    googleShare : function(){
        var url = window.location.href;
        this.windowPopup('https://plus.google.com/share?url='+url.trim(), 500, 300);
    },

    /**
     * Mail to
     * @param email
     * @param subject
     * @param message
     */
    mailTo : function(email, subject, message){
        var message = message.replace("<br>", "%0D%0A").replace("<br/>", "%0D%0A").replace("<br />", "%0D%0A");
        window.location.href = "mailto:"+email+"?subject="+subject+"&body="+message+"";
    },

    /**
     * Send mandrill mail
     * @param obj
     * @param callback
     */
    mandrillSend : function(obj, callback){
        console.log(obj);
        var data = {
            'key': obj.key,
            'message': {
                'from_email': obj.from_email,
                'to': [
                    {
                        'email': obj.email,
                        'name': obj.name,
                        'type': obj.type
                    }
                ],
                'subject': obj.subject,
                'html': obj.content
            }
        };

        request.post('https://mandrillapp.com/api/1.0/messages/send.json')
            .set('Content-Type', 'application/json')
            .send(data)
            .end(function(i, data){
                callback(data);
            });

    }

};